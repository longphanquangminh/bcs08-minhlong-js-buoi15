function bai1() {
  const diemChuan = document.getElementById("diem-chuan").value * 1;
  const khuVuc = document.getElementById("khu-vuc").value;
  const khuVucA = "A";
  const khuVucB = "B";
  const khuVucC = "C";
  let diemKhuVuc = 0;
  const doiTuong = document.getElementById("doi-tuong").value;
  const doiTuong1 = "1";
  const doiTuong2 = "2";
  const doiTuong3 = "3";
  let diemDoiTuong = 0;
  const diemMonThuNhat = document.getElementById("diem-mon-thu-nhat").value * 1;
  const diemMonThuHai = document.getElementById("diem-mon-thu-hai").value * 1;
  const diemMonThuBa = document.getElementById("diem-mon-thu-ba").value * 1;
  let thongBao = "";
  const thongBaoDau = "Bạn đã đậu. ";
  const thongBaoRot = "Bạn đã rớt. ";
  const lyDoDiem0 = "Do có một hoặc nhiều môn điểm 0!";
  const showTongDiem = "Tổng điểm: ";
  if (
    diemMonThuNhat < 0 ||
    diemMonThuNhat > 10 ||
    diemMonThuHai < 0 ||
    diemMonThuHai > 10 ||
    diemMonThuBa < 0 ||
    diemMonThuBa > 10 ||
    diemChuan <= 0 ||
    isNaN(diemMonThuNhat) ||
    isNaN(diemMonThuHai) ||
    isNaN(diemMonThuBa) ||
    isNaN(diemChuan)
  ) {
    thongBao =
      "Lỗi input! (Bạn cũng không được để trống các input cần thiết nhé)";
  } else if (diemMonThuNhat == 0 || diemMonThuHai == 0 || diemMonThuBa == 0) {
    thongBao = thongBaoRot + lyDoDiem0;
  } else {
    if (khuVuc == khuVucA) {
      diemKhuVuc = 2;
    } else if (khuVuc == khuVucB) {
      diemKhuVuc = 1;
    } else if (khuVuc == khuVucC) {
      diemKhuVuc = 0.5;
    }

    if (doiTuong == doiTuong1) {
      diemDoiTuong = 2.5;
    } else if (doiTuong == doiTuong2) {
      diemDoiTuong = 1.5;
    } else if (doiTuong == doiTuong3) {
      diemDoiTuong = 1;
    }

    diemTongKet =
      diemMonThuNhat + diemMonThuHai + diemMonThuBa + diemKhuVuc + diemDoiTuong;
    if (diemTongKet >= diemChuan) {
      thongBao = thongBaoDau + showTongDiem + diemTongKet;
    } else {
      thongBao = thongBaoRot + showTongDiem + diemTongKet;
    }
  }
  document.getElementById("ket-qua-bai-1").innerText = thongBao;
}

function capitalize(str) {
  // đối số đầu là regex (biểu thức chính quy) : https://regexr.com
  return str.replace(/\b\w/g, match => match.toUpperCase());
}

function bai2() {
  const hoTen = capitalize(document.getElementById("ho-ten").value);
  const soKw = document.getElementById("so-kw").value * 1;
  let tienDien = 0;
  let hienThi = 0;
  if (soKw <= 0 || isNaN(soKw)) {
    alert("Số kw chưa đúng!");
    hienThi = "LỖI!";
  } else {
    if (soKw <= 50) {
      tienDien = soKw * 500;
    } else if (soKw <= 100) {
      tienDien = 50 * 500 + (soKw - 50) * 650;
    } else if (soKw <= 200) {
      tienDien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    } else if (soKw <= 350) {
      tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    } else {
      tienDien =
        50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
    }
    hienThi = "Họ tên: " + hoTen + "; Tiền điện: " + tienDien.toLocaleString();
  }
  document.getElementById("ket-qua-bai-2").innerText = hienThi;
}

function bai3() {
  const hoTenCaNhan = capitalize(
    document.getElementById("ho-ten-ca-nhan").value,
  );
  const tongThuNhapNam = document.getElementById("tong-thu-nhap-nam").value * 1;
  const soNguoiPhuThuoc =
    document.getElementById("so-nguoi-phu-thuoc").value * 1;
  const thuNhapChiuThue = tongThuNhapNam - 4e6 - soNguoiPhuThuoc * 1.6e6;
  let thue = 0;
  let hienThi = "";
  if (
    tongThuNhapNam <= 0 ||
    isNaN(tongThuNhapNam) ||
    isNaN(soNguoiPhuThuoc) ||
    soNguoiPhuThuoc < 0
  ) {
    alert("Thu nhập hoặc số người chưa đúng!");
    hienThi = "LỖI!";
  } else {
    thue = tongThuNhapNam - 4e6 - soNguoiPhuThuoc * 16e5;
    if (thue <= 0) {
      alert("Thu nhập hoặc số người chưa đúng!");
      hienThi = "LỖI!";
    } else {
      if (thue <= 6e7) {
        thue = thuNhapChiuThue * 0.05;
      } else if (thue <= 12e7) {
        thue = thuNhapChiuThue * 0.1;
      } else if (thue <= 21e7) {
        thue = thuNhapChiuThue * 0.15;
      } else if (thue <= 384e6) {
        thue = thuNhapChiuThue * 0.2;
      } else if (thue <= 624e6) {
        thue = thuNhapChiuThue * 0.25;
      } else if (thue <= 96e7) {
        thue = thuNhapChiuThue * 0.3;
      } else {
        thue = thuNhapChiuThue * 0.35;
      }

      hienThi =
        "Họ tên: " +
        hoTenCaNhan +
        "; Tiền thuế thu nhập cá nhân: " +
        thue.toLocaleString() +
        " VND";
    }
  }
  document.getElementById("ket-qua-bai-3").innerText = hienThi;
}

function toggleInput() {
  const loaiKhachHang = document.getElementById("loai-khach-hang").value;
  const doanhNghiep = "doanh-nghiep";
  const soKetNoi = document.getElementById("so-ket-noi");

  if (loaiKhachHang == doanhNghiep) {
    soKetNoi.classList.remove("d-none");
    soKetNoi.disabled = false;
  } else {
    soKetNoi.classList.add("d-none");
    soKetNoi.disabled = true;
  }
}

function formatDecimal(number) {
  return number.toLocaleString(undefined, {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });
}

function bai4() {
  const loaiKhachHang = document.getElementById("loai-khach-hang").value;
  const maKhachHang = document.getElementById("ma-khach-hang").value;
  const soKenhCaoCap = document.getElementById("so-kenh-cao-cap").value * 1;
  const soKetNoi = document.getElementById("so-ket-noi").value * 1;
  const CHOOSE_NOTHING = "none";
  const nhaDan = "nha-dan";

  let phiXuLyHoaDon = 0;
  let phiDichVuCoBan = 0;
  let thueKenhCaoCap = 0;

  let tienCap = 0;
  let hienThi = "";

  if (
    loaiKhachHang == CHOOSE_NOTHING ||
    soKenhCaoCap < 0 ||
    isNaN(soKenhCaoCap)
  ) {
    alert("Vui lòng nhập đủ và đúng các thông tin!");
    hienThi = "Vui lòng nhập đủ và đúng các thông tin!";
  } else {
    if (loaiKhachHang == nhaDan) {
      phiXuLyHoaDon = 4.5;
      phiDichVuCoBan = 20.5;
      thueKenhCaoCap = 7.5;
    } else {
      phiXuLyHoaDon = 15;
      if (soKetNoi > 10) {
        phiDichVuCoBan = 75 + (soKetNoi - 10) * 5;
      } else {
        phiDichVuCoBan = 75;
      }
      thueKenhCaoCap = 50;
    }
    tienCap = phiXuLyHoaDon + phiDichVuCoBan + soKenhCaoCap * thueKenhCaoCap;
    hienThi =
      "Mã khách hàng: " +
      maKhachHang.toUpperCase() +
      "; Tiền cáp: " +
      new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
      }).format(tienCap);
  }

  document.getElementById("ket-qua-bai-4").innerText = hienThi;
}

document.addEventListener("keydown", function (event) {
  if (event.ctrlKey) {
    event.preventDefault();
  }
  if (event.key == "F12") {
    event.preventDefault();
  }
});

document.addEventListener("contextmenu", event => event.preventDefault());
